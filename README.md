# TempStorage

The package attempts to make it easier to programmatically work with temporary
files and directories.

[![Build Status](https://smokestone.visualstudio.com/Smokestone.TempStorage/_apis/build/status/TempStorage?branchName=master)](https://smokestone.visualstudio.com/Smokestone.TempStorage/_build/latest?definitionId=2&branchName=master)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=bugs)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=code_smells)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Coverage](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=coverage)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=ncloc)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=alert_status)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=security_rating)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=sqale_index)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68) [![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=a926159c2b7b260428fe070b623046996130ec68&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=a926159c2b7b260428fe070b623046996130ec68)

## Installing via NuGet

```PowerShell
Install-Package TempStorage
```

See [nuget.org/packages/TempStorage](https://www.nuget.org/packages/TempStorage/).

For integration with ASP.NET Core, see [TempStorage.AspNetCore](https://github.com/Smokestone/TempStorage.AspNetCore).

## Usage

A `TempStorage` instance functions as a kind of repository for temporary files
and directories. The parameterless constructor creates a new instance with
`System.IO.Path.GetTempPath()` as its root path. `TempStorage` is designed to
be used as a singleton during the lifetime of the application.

```cs
using ITempStorage tempStorage = new TempStorage();

using var ITempFile tempFile = tempStorage.NewTempFile();
using var ITempDirectory tempDirectory = tempStorage.NewTempDirectory();
```

The properties `ITempFile.Path` and `ITempDirectory.Path` point to the
actual locations on disk.

To delete a temporary file from disk, you simply dispose the `ITempFile` instance.
In the same way, you delete a temporary directory, along with any files and
subdirectories, by disposing the `ITempDirectory` instance.

When the `ITempStorage` itself is disposed, any undisposed `ITempFile` and
`ITempDirectory` instances are disposed.


## File Locks

In the event of the file being locked when the `ITempFile` instance is disposed, the
`Dispose()` method will return as normal while the actual delete operation is
delegated to a background worker. The background worker will try to delete the file
a configurable number of times (the default is 10) at a configurable interval (the
default is once every minute) before silently giving up. A temporary directory that
is locked, or contains locked files, is dealt with in the same way.

```cs
var options = new TempStorageOptions
{
    MaxNumberOfBackgroundDisposalAttempts = 3,
    BackgroundDisposalInterval = TimeSpan.FromSeconds(30D)
};
var tempStorage = new TempStorage(new OptionsWrapper<TempStorageOptions>(options));
```

Each `TempStorage` instance has its own background worker for dealing with locked
files and directories. When the `TempStorage` instance is disposed, the background
worker is consequently also disposed.


## Examples

A constructor overload lets you create a temporary storage with a custom
root path, instead of the default `System.IO.Path.GetTempPath()`:

```cs
var options = new TempStorageOptions {RootPath = @"C:\Temp"};
using ITempStorage tempStorage = new TempStorage(new OptionsWrapper<TempStorageOptions>(options));
    
// Any subdirectories and files that were created using the TempStorage instance
// are deleted when the instance is disposed. If C:\Temp did not exist when the
// TempStorage instance was constructed, C:\Temp is also deleted.
```

`ITempFile` has a bunch of convenience methods:

```cs
using ITempFile tempFile = tempStorage.NewTempFile();

// text content
tempFile.WriteAllText("some file content", Encoding.UTF8);
string contents = tempFile.ReadAllText(Encoding.UTF8);

// binary content
byte[] bytes = tempFile.ReadAllBytes();
tempFile.WriteAllBytes(bytes);

// streams
using (FileStream stream = tempFile.OpenRead())
{
    stream.Read(bytes);
}

using (FileStream stream = tempFile.OpenWrite())
{
    stream.Write(bytes);
}
```

Within a temporary directory, you can create temporary files and
subdirectories:

```cs
using ITempDirectory tempDir = tempStorage.NewTempDirectory();

using ITempFile tempFile = tempDir.NewTempFile();
tempFile.WriteAllText("some file content");

using ITempDirectory subDir = tempDir.NewTempDirectory();

// The directory along with any subdirectories and files are deleted
// when 'tempDir' is disposed.
```

The `ITempStorage.NewTempFile(string)` overload lets you create a temporary
file with a particular extension:

```cs
using ITempFile tempFile = tempStorage.NewTempFile("txt");
tempFile.WriteAllText("some file content");
```