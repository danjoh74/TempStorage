﻿namespace TempStorage
{
    /// <summary>Represents a storage for temporary files and directories.</summary>
    public interface ITempStorage : IFileSystemNode
    {
        /// <summary>Creates a new temporary file with a random name.</summary>
        /// <returns>The new temporary file</returns>
        ITempFile NewTempFile();

        /// <summary>Creates a new temporary file with a random name and the specified extension.</summary>
        /// <param name="extension">The extension, without leading period.</param>
        /// <returns>The new temporary file</returns>
        /// <example>tempStorage.NewTempFile("txt")</example>
        ITempFile NewTempFile(string extension);

        /// <summary>Creates a new temporary directory with a random name.</summary>
        /// <returns>The new temporary directory</returns>
        ITempDirectory NewTempDirectory();
    }
}