﻿using System;

namespace TempStorage
{
    /// <summary>Represents a file system node. A file system node is a file or directory.</summary>
    public interface IFileSystemNode : IDisposable
    {
        /// <summary>The path that leads to the file system node.</summary>
        string Path { get; }

        /// <summary>Raised when the instance is disposed.</summary>
        event EventHandler Disposed;
    }
}