﻿using System.IO;
using System.Text;

namespace TempStorage
{
    /// <summary>Represents a temporary file.</summary>
    public interface ITempFile : IFileSystemNode
    {
        /// <summary>Same as <see cref="System.IO.File.OpenRead" />.</summary>
        FileStream OpenRead();

        /// <summary>Same as <see cref="System.IO.File.OpenWrite" />.</summary>
        FileStream OpenWrite();

        /// <summary>Same as <see cref="System.IO.File.ReadAllBytes" />.</summary>
        byte[] ReadAllBytes();

        /// <summary>Same as <see cref="System.IO.File.WriteAllBytes" />.</summary>
        void WriteAllBytes(byte[] bytes);

        /// <summary>Same as <see cref="System.IO.File.ReadAllText" />.</summary>
        string ReadAllText();

        /// <summary>Same as <see cref="System.IO.File.ReadAllText" />.</summary>
        string ReadAllText(Encoding encoding);

        /// <summary>Same as <see cref="System.IO.File.WriteAllText" />.</summary>
        void WriteAllText(string contents);

        /// <summary>Same as <see cref="System.IO.File.WriteAllText" />.</summary>
        void WriteAllText(string contents, Encoding encoding);
    }
}