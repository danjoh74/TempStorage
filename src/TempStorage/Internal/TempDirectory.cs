﻿using System;
using System.Collections.Concurrent;
using System.IO;

namespace TempStorage.Internal
{
    internal class TempDirectory : FileSystemNode, ITempDirectory
    {
        private readonly IGarbageDisposal garbageDisposal;
        private readonly Func<string> getRandomFileSystemNodeName;
        private readonly bool isStorageOwner;

        private readonly ConcurrentDictionary<object, IFileSystemNode> items =
            new ConcurrentDictionary<object, IFileSystemNode>();

        public TempDirectory(
            string path, Func<string> getRandomFileSystemNodeName, IGarbageDisposal garbageDisposal)
            : base(path, garbageDisposal)
        {
            this.getRandomFileSystemNodeName = getRandomFileSystemNodeName;
            this.garbageDisposal = garbageDisposal;
            isStorageOwner = !Directory.Exists(path);
            Directory.CreateDirectory(path);
        }

        public ITempFile NewTempFile()
        {
            return NewTempFile("tmp");
        }

        public ITempFile NewTempFile(string extension)
        {
            while (true)
                try
                {
                    var tempFilePath = System.IO.Path.Combine(
                        Path, $"{getRandomFileSystemNodeName()}.{(extension ?? string.Empty).TrimStart('.')}");
                    new FileStream(tempFilePath, FileMode.CreateNew).Dispose();

                    return Track(new TempFile(tempFilePath, garbageDisposal));
                }
                catch (IOException exception)
                {
                    if ((exception.HResult & 0x0000FFFF) != 80)
                        throw;
                }
        }

        public ITempDirectory NewTempDirectory()
        {
            string tempDirPath;
            do
            {
                tempDirPath = System.IO.Path.Combine(Path, getRandomFileSystemNodeName());
            } while (Directory.Exists(tempDirPath));

            return Track(new TempDirectory(tempDirPath, getRandomFileSystemNodeName, garbageDisposal));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                var keys = items.Keys;
                foreach (var key in keys)
                    if (items.TryRemove(key, out var item))
                        item.Dispose();
            }

            base.Dispose(disposing);
        }

        protected override void DeleteFromDisk()
        {
            if (isStorageOwner && Directory.Exists(Path))
                Directory.Delete(Path, true);
        }

        private T Track<T>(T item) where T : IFileSystemNode
        {
            items[item] = item;
            item.Disposed += (_, __) => items.TryRemove(item, out var ___);
            return item;
        }
    }
}