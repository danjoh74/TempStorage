﻿using System.IO;
using System.Text;

namespace TempStorage.Internal
{
    internal class TempFile : FileSystemNode, ITempFile
    {
        public TempFile(string path, IGarbageDisposal garbageDisposal) : base(path, garbageDisposal)
        {
        }

        public FileStream OpenRead()
        {
            return File.OpenRead(Path);
        }

        public FileStream OpenWrite()
        {
            return File.OpenWrite(Path);
        }

        public byte[] ReadAllBytes()
        {
            return File.ReadAllBytes(Path);
        }

        public void WriteAllBytes(byte[] bytes)
        {
            File.WriteAllBytes(Path, bytes);
        }

        public string ReadAllText()
        {
            return File.ReadAllText(Path);
        }

        public string ReadAllText(Encoding encoding)
        {
            return File.ReadAllText(Path, encoding);
        }

        public void WriteAllText(string contents)
        {
            File.WriteAllText(Path, contents);
        }

        public void WriteAllText(string contents, Encoding encoding)
        {
            File.WriteAllText(Path, contents, encoding);
        }

        protected override void DeleteFromDisk()
        {
            if (File.Exists(Path))
                File.Delete(Path);
        }
    }
}