﻿using System;

namespace TempStorage.Internal
{
    internal abstract class FileSystemNode : IFileSystemNode
    {
        private readonly IGarbageDisposal garbageDisposal;

        protected FileSystemNode(string path, IGarbageDisposal garbageDisposal)
        {
            Path = path;
            this.garbageDisposal = garbageDisposal;
        }

        public string Path { get; }

        public event EventHandler Disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected abstract void DeleteFromDisk();

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    DeleteFromDisk();
                }
                catch
                {
                    garbageDisposal.Add(DeleteFromDisk);
                }

                Disposed?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}