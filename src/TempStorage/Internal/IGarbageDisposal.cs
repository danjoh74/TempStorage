﻿using System;

namespace TempStorage.Internal
{
    internal interface IGarbageDisposal : IDisposable
    {
        void Add(Action dispose);
    }
}