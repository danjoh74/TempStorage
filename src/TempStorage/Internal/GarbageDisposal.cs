﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Extensions.Options;

namespace TempStorage.Internal
{
    internal sealed class GarbageDisposal : IGarbageDisposal
    {
        private readonly Timer disposalTimer;
        private readonly ConcurrentQueue<DisposalItem> garbage = new ConcurrentQueue<DisposalItem>();
        private readonly IOptions<TempStorageOptions> options;

        public GarbageDisposal(IOptions<TempStorageOptions> options)
        {
            this.options = options;

            disposalTimer = new Timer(
                RunDisposer, null, options.Value.BackgroundDisposalInterval, options.Value.BackgroundDisposalInterval);
        }

        public void Add(Action dispose)
        {
            garbage.Enqueue(new DisposalItem(dispose));
        }

        public void Dispose()
        {
            disposalTimer?.Dispose();
        }

        private void RunDisposer(object _)
        {
            RunDisposer().ToList().ForEach(item =>
            {
                if (++item.Attempts < options.Value.MaxNumberOfBackgroundDisposalAttempts)
                    garbage.Enqueue(item);
            });
            disposalTimer.Change(options.Value.BackgroundDisposalInterval, options.Value.BackgroundDisposalInterval);
        }

        private IEnumerable<DisposalItem> RunDisposer()
        {
            while (garbage.TryDequeue(out var item))
                if (!TryDispose(item))
                    yield return item;
        }

        private static bool TryDispose(DisposalItem item)
        {
            try
            {
                item.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private class DisposalItem
        {
            public DisposalItem(Action dispose)
            {
                Dispose = dispose;
            }

            public Action Dispose { get; }
            public int Attempts { get; set; }
        }
    }
}