﻿namespace TempStorage
{
    /// <summary>Represents a temporary directory.</summary>
    public interface ITempDirectory : ITempStorage
    {
    }
}