﻿using System;
using System.IO;

namespace TempStorage
{
    public class TempStorageOptions
    {
        public static TempStorageOptions Default => new TempStorageOptions();

        public string RootPath { get; set; } = Path.GetTempPath();
        public TimeSpan BackgroundDisposalInterval { get; set; } = TimeSpan.FromMinutes(1D);
        public int MaxNumberOfBackgroundDisposalAttempts { get; set; } = 10;
    }
}