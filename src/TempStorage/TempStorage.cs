﻿using System;
using Microsoft.Extensions.Options;
using TempStorage.Internal;

namespace TempStorage
{
    public sealed class TempStorage : ITempStorage
    {
        private readonly ITempDirectory directory;
        private readonly IGarbageDisposal garbageDisposal;

        public TempStorage()
            : this(System.IO.Path.GetRandomFileName, new OptionsWrapper<TempStorageOptions>(TempStorageOptions.Default))
        {
        }

        public TempStorage(IOptions<TempStorageOptions> options)
            : this(
                System.IO.Path.GetRandomFileName,
                options ?? new OptionsWrapper<TempStorageOptions>(TempStorageOptions.Default))
        {
        }

        private TempStorage(Func<string> getRandomFileSystemNodeName, IOptions<TempStorageOptions> options)
            : this(getRandomFileSystemNodeName, options, new GarbageDisposal(options))
        {
        }

        private TempStorage(
            Func<string> getRandomFileSystemNodeName, IOptions<TempStorageOptions> options,
            IGarbageDisposal garbageDisposal)
            : this(new TempDirectory(options.Value.RootPath, getRandomFileSystemNodeName, garbageDisposal))
        {
            this.garbageDisposal = garbageDisposal;
        }

        internal TempStorage(ITempDirectory directory)
        {
            this.directory = directory;
        }

        public string Path => directory.Path;

        public event EventHandler Disposed;

        public ITempFile NewTempFile()
        {
            return directory.NewTempFile();
        }

        public ITempFile NewTempFile(string extension)
        {
            return directory.NewTempFile(extension);
        }

        public ITempDirectory NewTempDirectory()
        {
            return directory.NewTempDirectory();
        }

        public void Dispose()
        {
            directory?.Dispose();
            garbageDisposal?.Dispose();

            OnDisposed();
        }

        private void OnDisposed()
        {
            Disposed?.Invoke(this, EventArgs.Empty);
        }
    }
}