﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("TempStorage.UnitTests")]
[assembly: InternalsVisibleTo("TempStorage.IntegrationTests")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]