﻿using System;
using System.IO;
using System.Text;
using Microsoft.Extensions.Options;

namespace TempStorage.Samples.ReadmeExamples
{
    internal static class Program
    {
        private static void Main()
        {
            using ITempStorage tempStorage = new TempStorage();

            CreateTemporaryFile(tempStorage);
            CreateTemporaryFileWithExtension(tempStorage);
            CreateTemporaryDirectory(tempStorage);
            CreateTemporaryStorageWithCustomRootPath();
            TempFileConvenienceMethods(tempStorage);
            BackgroundDisposalConfiguration();
        }

        private static void CreateTemporaryFile(ITempStorage tempStorage)
        {
            using ITempFile tempFile = tempStorage.NewTempFile();
            tempFile.WriteAllBytes(new byte[] {0x2a});
        }

        private static void CreateTemporaryFileWithExtension(ITempStorage tempStorage)
        {
            using ITempFile tempFile = tempStorage.NewTempFile("txt");
            tempFile.WriteAllText("some file content");
        }

        private static void CreateTemporaryDirectory(ITempStorage tempStorage)
        {
            using ITempDirectory tempDir = tempStorage.NewTempDirectory();

            using ITempFile tempFile = tempDir.NewTempFile();
            tempFile.WriteAllText("some file content");

            using ITempDirectory subDir = tempDir.NewTempDirectory();
        }

        private static void CreateTemporaryStorageWithCustomRootPath()
        {
            var options = new TempStorageOptions {RootPath = @"C:\Temp"};
            using ITempStorage tempStorage = new TempStorage(
                new OptionsWrapper<TempStorageOptions>(options));
        }

        private static void TempFileConvenienceMethods(ITempStorage tempStorage)
        {
            using ITempFile tempFile = tempStorage.NewTempFile();

            // text content
            tempFile.WriteAllText("some file content", Encoding.UTF8);
            string contents = tempFile.ReadAllText(Encoding.UTF8);

            // binary content
            byte[] bytes = tempFile.ReadAllBytes();
            tempFile.WriteAllBytes(bytes);

            // streams
            using (FileStream stream = tempFile.OpenRead())
            {
                stream.Read(bytes);
            }

            using (FileStream stream = tempFile.OpenWrite())
            {
                stream.Write(bytes);
            }
        }

        private static void BackgroundDisposalConfiguration()
        {
            var options = new TempStorageOptions
            {
                MaxNumberOfBackgroundDisposalAttempts = 3,
                BackgroundDisposalInterval = TimeSpan.FromSeconds(30D)
            };
            var tempStorage = new TempStorage(new OptionsWrapper<TempStorageOptions>(options));
        }
    }
}