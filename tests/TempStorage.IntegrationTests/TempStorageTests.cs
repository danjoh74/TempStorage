using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Xunit;

namespace TempStorage.IntegrationTests
{
    [Trait("Type", "Integration Test")]
    public class TempStorageTests : IDisposable
    {
        public TempStorageTests()
        {
            var options = new TempStorageOptions
            {
                RootPath = Guid.NewGuid().ToString("N"),
                BackgroundDisposalInterval = TimeSpan.FromSeconds(.1),
                MaxNumberOfBackgroundDisposalAttempts = 100
            };
            subject = new TempStorage(new OptionsWrapper<TempStorageOptions>(options));
        }

        public void Dispose()
        {
            subject?.Dispose();
        }

        private readonly TempStorage subject;

        [Fact]
        public async Task
            Disposing_a_temp_directory_that_is_locked_should_eventually_delete_the_directory_from_disk_when_the_lock_is_released()
        {
            string tempDirPath;
            Stream tempFileLock;
            using (var tempDir = subject.NewTempDirectory())
            {
                tempDirPath = tempDir.Path;

                // open a stream to lock the file
                var filePath = Path.Combine(tempDirPath, "test.dat");
                tempFileLock = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None);
            }

            await Task.Delay(TimeSpan.FromSeconds(1D));
            Debug.Assert(Directory.Exists(tempDirPath));

            // release the file lock
            tempFileLock.Dispose();

            var stopwatch = Stopwatch.StartNew();
            while (stopwatch.Elapsed < TimeSpan.FromSeconds(5D) && Directory.Exists(tempDirPath))
            {
            }

            Directory.Exists(tempDirPath).Should().BeFalse();
        }

        [Fact]
        public async Task
            Disposing_a_temp_file_that_is_locked_should_eventually_delete_the_file_from_disk_when_the_lock_is_released()
        {
            string tempFilePath;
            Stream tempFileLock;
            using (var tempFile = subject.NewTempFile())
            {
                tempFilePath = tempFile.Path;

                // open a stream to lock the file
                tempFileLock = new FileStream(tempFile.Path, FileMode.Append, FileAccess.Write, FileShare.None);
            }

            await Task.Delay(TimeSpan.FromSeconds(1D));
            Debug.Assert(File.Exists(tempFilePath));

            // release the file lock
            tempFileLock.Dispose();


            var stopwatch = Stopwatch.StartNew();
            while (stopwatch.Elapsed < TimeSpan.FromSeconds(5D) && File.Exists(tempFilePath))
            {
            }

            File.Exists(tempFilePath).Should().BeFalse();
        }
    }
}