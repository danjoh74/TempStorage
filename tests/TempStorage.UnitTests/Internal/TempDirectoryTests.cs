﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using FluentAssertions;
using NSubstitute;
using TempStorage.Internal;
using Xunit;

namespace TempStorage.UnitTests.Internal
{
    [Trait("Type", "Unit Test")]
    public class TempDirectoryTests : IDisposable
    {
        public TempDirectoryTests()
        {
            garbageDisposalMock = Substitute.For<IGarbageDisposal>();
            path = Guid.NewGuid().ToString("N");
            subject = new TempDirectory(path, Path.GetRandomFileName, garbageDisposalMock);
        }

        public void Dispose()
        {
            subject?.Dispose();
        }

        private readonly TempDirectory subject;
        private readonly IGarbageDisposal garbageDisposalMock;
        private readonly string path;

        [Fact]
        public void Disposing_a_temp_directory_multiple_times_should_not_throw_exception()
        {
            Action act = () =>
            {
                subject.Dispose();
                subject.Dispose();
            };

            act.Should().NotThrow();
        }

        [Fact]
        public void Disposing_a_temp_directory_should_delete_the_directory_from_disk()
        {
            var tempDirPath = subject.Path;
            File.WriteAllText(Path.Combine(tempDirPath, "test.txt"), "test");
            subject.Dispose();

            Directory.Exists(tempDirPath).Should().BeFalse();
        }

        [Fact]
        public void Disposing_a_temp_directory_that_is_locked_should_add_to_garbage_disposal()
        {
            using var callReceived = new ManualResetEvent(false);
            garbageDisposalMock.When(disposal => disposal.Add(Arg.Any<Action>())).Do(_ => callReceived.Set());
            // open a stream to lock the file
            var filePath = Path.Combine(subject.Path, "test.dat");
            Stream tempFileLock = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None);
            subject.Dispose();

            // release the file lock
            tempFileLock.Dispose();

            callReceived.WaitOne(TimeSpan.FromSeconds(1D));

            garbageDisposalMock.Received(1).Add(Arg.Any<Action>());
        }

        [Fact]
        public void Disposing_the_temp_directory_should_delete_all_its_subdirectories_from_disk()
        {
            var subDirectories = new List<string>();
            subDirectories.AddRange(Enumerable.Range(0, 10).Select(_ => subject.NewTempDirectory().Path));
            foreach (var subDirectory in subDirectories)
            {
                var filePath = Path.Combine(subDirectory, "test.txt");
                File.WriteAllText(filePath, "test");
            }

            subject.Dispose();

            subDirectories.Any(Directory.Exists).Should().BeFalse();
        }

        [Fact]
        public void Disposing_the_temp_directory_should_delete_all_its_temp_files_from_disk()
        {
            var tempFiles = new List<string>();
            tempFiles.AddRange(Enumerable.Range(0, 10).Select(_ => subject.NewTempFile().Path));

            subject.Dispose();

            tempFiles.Any(File.Exists).Should().BeFalse();
        }

        [Fact]
        public void
            Disposing_the_temp_directory_should_delete_the_root_directory_from_disk_if_it_did_not_exist_beforehand()
        {
            if (Directory.Exists(path))
                Directory.Delete(path, true);
            var tempDirectory = new TempDirectory(path, () => "", garbageDisposalMock);

            tempDirectory.Dispose();

            Directory.Exists(path).Should().BeFalse();
        }

        [Fact]
        public void
            Disposing_the_temp_directory_should_not_delete_the_root_directory_from_disk_if_it_existed_beforehand()
        {
            Directory.CreateDirectory(path);
            var repository = new TempDirectory(path, () => "", garbageDisposalMock);

            repository.Dispose();

            Directory.Exists(path).Should().BeTrue();
        }

        [Fact]
        public void Getting_a_new_subdirectory_should_create_a_new_unique_directory_on_disk()
        {
            var i = 0;
            var existing = Path.GetRandomFileName();

            string GetRandomFileName()
            {
                return i++ <= 1 ? existing : Path.GetRandomFileName();
            }

            var tempDirectory = new TempDirectory(path, GetRandomFileName, garbageDisposalMock);
            using var _ = tempDirectory.NewTempDirectory();

            using var tempDir = tempDirectory.NewTempDirectory();

            Directory.Exists(tempDir.Path).Should().BeTrue();
        }

        [Fact]
        public void Getting_a_new_subdirectory_should_generate_a_new_unique_directory_name()
        {
            var i = 0;
            var existing = Path.GetRandomFileName();

            string GetRandomFileName()
            {
                return i++ <= 1 ? existing : Path.GetRandomFileName();
            }

            var tempDirectory = new TempDirectory(path, GetRandomFileName, garbageDisposalMock);
            using var existingTempDir = tempDirectory.NewTempDirectory();

            using var tempDir = tempDirectory.NewTempDirectory();

            tempDir.Path.Should().NotBe(existingTempDir.Path);
        }

        [Fact]
        public void Getting_a_new_temp_file_should_create_a_new_unique_file_on_disk()
        {
            var i = 0;
            var existing = Path.GetRandomFileName();

            string GetRandomFileName()
            {
                return i++ <= 1 ? existing : Path.GetRandomFileName();
            }

            var tempDirectory = new TempDirectory(path, GetRandomFileName, garbageDisposalMock);
            using var _ = tempDirectory.NewTempFile();

            using var tempFile = tempDirectory.NewTempFile();

            File.Exists(tempFile.Path).Should().BeTrue();
        }

        [Fact]
        public void Getting_a_new_temp_file_should_generate_a_new_unique_file_name()
        {
            var i = 0;
            var existing = Path.GetRandomFileName();

            string GetRandomFileName()
            {
                return i++ <= 1 ? existing : Path.GetRandomFileName();
            }

            var tempDirectory = new TempDirectory(path, GetRandomFileName, garbageDisposalMock);
            using var existingTempFile = tempDirectory.NewTempFile();

            using var tempFile = tempDirectory.NewTempFile();

            tempFile.Path.Should().NotBe(existingTempFile.Path);
        }

        [Fact]
        public void Getting_a_new_temp_file_with_a_specific_extension_containing_leading_dot_should_ignore_leading_dot()
        {
            using var tempFile = subject.NewTempFile(".txt");
            tempFile.Path.Should().NotEndWith("..txt");
        }

        [Fact]
        public void Getting_a_new_temp_file_with_a_specific_extension_should_create_the_file_on_disk()
        {
            using var tempFile = subject.NewTempFile("txt");
            File.Exists(tempFile.Path).Should().BeTrue();
        }

        [Fact]
        public void
            Getting_a_new_temp_file_with_a_specific_extension_should_generate_a_file_name_with_the_specified_extension()
        {
            using var tempFile = subject.NewTempFile("json");
            Path.GetExtension(tempFile.Path).Should().Be(".json");
        }

        [Fact]
        public void Getting_a_new_temp_file_with_invalid_name_should_fail()
        {
            Action act = () => new TempDirectory(path, () => @"/\", garbageDisposalMock).NewTempFile();

            act.Should().Throw<IOException>();
        }

        [Fact]
        public void Getting_a_new_temp_file_with_null_as_extension_should_result_in_a_temp_file_without_extension()
        {
            using var tempFile = subject.NewTempFile(null);
            Path.GetExtension(tempFile.Path).Should().BeEmpty();
        }
    }
}