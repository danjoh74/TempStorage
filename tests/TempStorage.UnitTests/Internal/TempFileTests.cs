﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using AutoFixture;
using FluentAssertions;
using NSubstitute;
using TempStorage.Internal;
using Xunit;

namespace TempStorage.UnitTests.Internal
{
    [Trait("Type", "Unit Test")]
    public class TempFileTests : IDisposable
    {
        public TempFileTests()
        {
            garbageDisposalMock = Substitute.For<IGarbageDisposal>();
            subject = new TempFile(Path.GetTempFileName(), garbageDisposalMock);
        }

        public void Dispose()
        {
            subject?.Dispose();
        }

        private static readonly Fixture Fixture = new Fixture();
        private readonly IGarbageDisposal garbageDisposalMock;
        private readonly TempFile subject;

        [Fact]
        public void Disposing_a_temp_file_multiple_times_should_not_throw_exception()
        {
            Action act = () =>
            {
                subject.Dispose();
                subject.Dispose();
            };

            act.Should().NotThrow();
        }

        [Fact]
        public void Disposing_a_temp_file_should_delete_the_file_from_disk()
        {
            var tempFilePath = subject.Path;
            subject.Dispose();

            File.Exists(tempFilePath).Should().BeFalse();
        }

        [Fact]
        public void Disposing_a_temp_file_that_is_locked_should_add_to_garbage_disposal()
        {
            using var callReceived = new ManualResetEvent(false);
            garbageDisposalMock.When(disposal => disposal.Add(Arg.Any<Action>())).Do(_ => callReceived.Set());
            // open a stream to lock the file
            Stream tempFileLock = new FileStream(subject.Path, FileMode.Append, FileAccess.Write, FileShare.None);
            subject.Dispose();

            // release the file lock
            tempFileLock.Dispose();

            callReceived.WaitOne(TimeSpan.FromSeconds(1D));

            garbageDisposalMock.Received(1).Add(Arg.Any<Action>());
        }

        [Fact]
        public void Open_a_temp_file_for_reading_should_open_a_readable_stream_to_the_file()
        {
            var expected = Fixture.CreateMany<byte>().ToArray();
            File.WriteAllBytes(subject.Path, expected);

            var actual = new byte[expected.Length];
            using var stream = subject.OpenRead();
            stream.Read(actual);

            actual.Should().Equal(expected);
        }

        [Fact]
        public void Open_a_temp_file_for_writing_should_open_a_writable_stream_to_the_file()
        {
            var expected = Fixture.CreateMany<byte>().ToArray();

            using (var stream = subject.OpenWrite())
            {
                stream.Write(expected);
            }

            var actual = File.ReadAllBytes(subject.Path);
            actual.Should().Equal(expected);
        }

        [Fact]
        public void Reading_all_bytes_from_a_temp_file_should_read_file_content()
        {
            var expected = Fixture.CreateMany<byte>().ToArray();
            File.WriteAllBytes(subject.Path, expected);

            var actual = subject.ReadAllBytes();

            actual.Should().Equal(expected);
        }

        [Fact]
        public void Reading_all_text_from_a_temp_file_should_read_file_content()
        {
            var expected = Fixture.Create<string>();
            File.WriteAllText(subject.Path, expected);

            var actual = subject.ReadAllText();

            actual.Should().Be(expected);
        }

        [Fact]
        public void Reading_all_text_with_specified_encoding_from_a_temp_file_should_read_file_content()
        {
            var expected = Fixture.Create<string>();
            File.WriteAllText(subject.Path, expected, Encoding.UTF32);

            var actual = subject.ReadAllText(Encoding.UTF32);

            actual.Should().Be(expected);
        }

        [Fact]
        public void Writing_all_bytes_to_a_temp_file_should_write_file_content()
        {
            var expected = Fixture.CreateMany<byte>().ToArray();

            subject.WriteAllBytes(expected);

            var actual = File.ReadAllBytes(subject.Path);
            actual.Should().Equal(expected);
        }

        [Fact]
        public void Writing_all_text_to_a_temp_file_should_write_file_content()
        {
            var expected = Fixture.Create<string>();

            subject.WriteAllText(expected);

            var actual = File.ReadAllText(subject.Path);
            actual.Should().Be(expected);
        }

        [Fact]
        public void Writing_all_text_with_specified_encoding_to_a_temp_file_should_write_file_content()
        {
            var expected = Fixture.Create<string>();

            subject.WriteAllText(expected, Encoding.ASCII);

            var actual = File.ReadAllText(subject.Path, Encoding.ASCII);
            actual.Should().Be(expected);
        }
    }
}