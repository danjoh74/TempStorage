﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Options;
using TempStorage.Internal;
using Xunit;

namespace TempStorage.UnitTests.Internal
{
    [Trait("Type", "Unit Test")]
    public class GarbageDisposalTests
    {
        public GarbageDisposalTests()
        {
            subject = new GarbageDisposal(
                new OptionsWrapper<TempStorageOptions>(
                    new TempStorageOptions
                    {
                        BackgroundDisposalInterval = TimeSpan.FromSeconds(.1),
                        MaxNumberOfBackgroundDisposalAttempts = 10
                    }));
        }

        private readonly GarbageDisposal subject;

        [Fact]
        public async Task GarbageDisposal_should_not_perform_more_disposal_attempts_than_necessary()
        {
            var count = 0;
            var dispose = new Action(() =>
            {
                if (++count < 5)
                    throw new Exception();
            });

            subject.Add(dispose);

            await Task.Delay(TimeSpan.FromSeconds(1));

            count.Should().Be(5);
        }

        [Fact]
        public async Task GarbageDisposal_should_perform_configured_number_of_disposal_attempts()
        {
            var count = 0;
            var dispose = new Action(() =>
            {
                count++;
                throw new Exception();
            });

            subject.Add(dispose);

            await Task.Delay(TimeSpan.FromSeconds(1.5));

            count.Should().Be(10);
        }
    }
}