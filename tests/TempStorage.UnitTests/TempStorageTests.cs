using System;
using System.IO;
using FluentAssertions;
using NSubstitute;
using Xunit;

namespace TempStorage.UnitTests
{
    [Trait("Type", "Unit Test")]
    public class TempStorageTests : IDisposable
    {
        public TempStorageTests()
        {
            rootDirectoryMock = Substitute.For<ITempDirectory>();
            subject = new TempStorage(rootDirectoryMock);
        }

        public void Dispose()
        {
            rootDirectoryMock?.Dispose();
            subject?.Dispose();
        }

        private readonly TempStorage subject;
        private readonly ITempDirectory rootDirectoryMock;

        [Fact]
        public void Disposing_the_temp_storage_should_dispose_the_root_directory()
        {
            subject.Dispose();

            rootDirectoryMock.Received(1).Dispose();
        }

        [Fact]
        public void Getting_a_new_temp_directory_should_create_a_new_subdirectory_in_the_root_directory()
        {
            var expected = Substitute.For<ITempDirectory>();
            rootDirectoryMock.NewTempDirectory().Returns(expected);

            var actual = subject.NewTempDirectory();

            actual.Should().BeSameAs(expected);
        }

        [Fact]
        public void Getting_a_new_temp_file_should_create_a_new_file_in_the_root_directory()
        {
            var expected = Substitute.For<ITempFile>();
            rootDirectoryMock.NewTempFile().Returns(expected);

            var actual = subject.NewTempFile();

            actual.Should().BeSameAs(expected);
        }

        [Fact]
        public void Getting_a_new_temp_file_with_specific_extension_should_create_a_new_file_in_the_root_directory()
        {
            var expected = Substitute.For<ITempFile>();
            rootDirectoryMock.NewTempFile("xml").Returns(expected);

            var actual = subject.NewTempFile("xml");

            actual.Should().BeSameAs(expected);
        }

        [Fact]
        public void Creating_a_temp_storage_should_set_path_to_current_users_temporary_folder()
        {
            var tempStorage = new TempStorage();

            tempStorage.Path.Should().Be(Path.GetTempPath());
        }
    }
}